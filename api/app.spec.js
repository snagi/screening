const chai = require("chai");
const chaiHttp = require("chai-http");

const app = require("./app");

const should = chai.should();
chai.use(chaiHttp);

describe("App tests", () => {
  describe("GET Status", () => {
    it("should return status of application", (done) => {
      chai
        .request(app)
        .get("/api/status")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          done();
        });
    });
  });
});
