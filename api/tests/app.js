const chai = require("chai");
const chaiHttp = require("chai-http");

const should = chai.should();
chai.use(chaiHttp);

describe("App tests", () => {
  describe("GET Status", () => {
    it("should return status of application", (done) => {
      chai
        .request(process.env.API_HOST)
        .get("/api/status")
        .end((err, res) => {
          console.log("res:status = ", res.status, "res:body = ", res.body);
          res.should.have.status(200);
          res.body.should.be.a("array");
          done();
        });
    });
  });
});
