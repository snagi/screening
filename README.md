# Sample 3tier app
This repo contains code for a Node.js multi-tier application.

The application overview is as follows

```
web <=> api <=> db
```

The folders `web` and `api` respectively describe how to install and run each app.


# Deployment architecture
CI/CD pipeline deploys the infrastructure on Digital Ocean by creating a kubernetes cluster and exposing the cluster ingress using a loadbalancer server. The cluster uses a autoscaling node pool with 3 minimum nodes to support high availability of components. Prometheus, Grafana, Loki, kube state metrics, node exporter and promtail are used for observability of infrastructure.

![Architecture](./docs/3tier-app.drawio.svg)

# CI/CD Pipeline
CI/CD pipeline uses GitLab native capability for pipeline as code and runners. This repository is mirrored at https://gitlab.com/snagi/screening for pipeline execution.

Latest executions can be seen at https://gitlab.com/snagi/screening/-/pipelines

![CI/CD Pipeline](./docs/ci-pipeline.png)
