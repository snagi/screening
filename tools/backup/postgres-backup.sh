#!/bin/bash

date1=$(date +%Y%m%d-%H%M)
mkdir pg-backup
PGPASSWORD="$DBPASS" pg_dumpall --host $DBHOST --port $DBPORT --username $DBUSER --database $DB > pg-backup/postgres-db.tar
file_name="pg-backup-"$date1".tar.gz"

#Compressing backup file for upload
tar -zcvf $file_name pg-backup

status_msg="failed"
filesize=$(stat -c %s $file_name)
mfs=10
if [[ "$filesize" -gt "$mfs" ]]; then
# Uploading to s3
aws s3 cp --endpoint=https://$AWS_DEFAULT_REGION.digitaloceanspaces.com pg-backup-$date1.tar.gz $BUCKET_NAME
status_msg="successful"
fi

echo "Postgres database backup status: status_msg"
