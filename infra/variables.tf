variable "region" {
  type = string
  default = "lon1"
}
variable "project_name" {
  type = string
  default = "screening"
}
variable "site_domain" {
  type = string
  default = "snagi.me"
}

variable "WEB_IMAGE" {
  type = string
  default = "registry.gitlab.com/snagi/screening/screening-web:latest"
}
variable "API_IMAGE" {
  type = string
  default = "registry.gitlab.com/snagi/screening/screening-api:latest"
}
variable "BACKUP_TOOLS_IMAGE" {
  type = string
  default = "registry.gitlab.com/snagi/screening/screening-backup-tools:latest"
}
variable "SPACES_ACCESS_KEY_ID" {
  type = string
}
variable "SPACES_SECRET_ACCESS_KEY" {
  type = string
}
variable "PG_BACKUP_BUCKET" {
  type = string
  default = "postgres-backup-bucket"
}