variable "cluster_host" {
  type = string
}
variable "cluster_name" {
  type = string
}
variable "kube_config" {
  type = object({
   token = string,
   cluster_ca_certificate = string,
  })
}
variable "ingress_hostname" {
  type = string
}
variable "cert_issuer" {
  type = string
}
variable "namespace" {
  type = string
}

variable "db_name" {
  type = string
}
variable "db_host" {
  type = string
}
variable "db_port" {
  type = string
}
variable "db_user" {
  type = string
}
variable "db_password" {
  type = string
}

variable "web_image" {
  type = string
}
variable "api_image" {
  type = string
}
variable "backup_tools_image" {
  type = string
}