terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

provider "kubernetes" {
  host  = var.cluster_host
  token = var.kube_config.token
  cluster_ca_certificate = base64decode(
    var.kube_config.cluster_ca_certificate
  )
}

provider "kubectl" {
  host  = var.cluster_host
  token = var.kube_config.token
  cluster_ca_certificate = base64decode(
    var.kube_config.cluster_ca_certificate
  )
  load_config_file = false
}

resource "kubernetes_namespace" "ns" {
  metadata {
    name = var.namespace
  }
}

locals {
  app_api = "screening-api"
  svc_api = "screening-api"
}

module "screening-web" {
  source = "./application"

  cert_issuer = var.cert_issuer
  namespace = var.namespace
  app_name = "screening-web"
  svc_name = "screening-web"
  image = var.web_image
  app_port = "8080"
  probe_path = "/"
  ingress_domain = "web.${var.ingress_hostname}"
  cdn_domain = "screening.snagi.me"
  ingress_path = "/"

  env = [
    {
      name = "API_HOST"
      value = "http://screening-api"
    }
  ]
}

module "screening-api" {
  source = "./application"

  cert_issuer = var.cert_issuer
  namespace = var.namespace
  app_name = "screening-api"
  svc_name = "screening-api"
  image = var.api_image
  app_port = "8080"
  probe_path = "/api/status"
  ingress_domain = "api.${var.ingress_hostname}"
  cdn_domain = "screening.snagi.me"
  ingress_path = "/api/"

  env = [
    {
      name = "DB"
      value = var.db_name
    },
    {
      name = "DBHOST"
      value = var.db_host
    },
    {
      name = "DBPORT"
      value = var.db_port
    },
    {
      name = "DBUSER"
      value = var.db_user
    },
    {
      name = "DBPASS"
      value = var.db_password
    }
  ]
}
