variable "cert_issuer" {
  type = string
}
variable "namespace" {
  type = string
}
variable "cdn_domain" {
  type = string
}
variable "ingress_domain" {
  type = string
}
variable "ingress_path" {
  type = string
  default = ""
}
variable "app_name" {
  type = string
}
variable "image" {
  type = string
}
variable "image_pull_policy" {
  type = string
  default = "Always"
}
variable "env" {
  type = list(object({
    name = string, 
    value = string
  }))
  default = []
}
variable "app_port" {
  type = string
}
variable "probe_path" {
  type = string
}
variable "svc_name" {
  type = string
}