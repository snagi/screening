terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
  }
}

# resource "kubernetes_namespace" "ns" {
#   metadata {
#     name = var.namespace
#   }
# }

resource "kubernetes_deployment" "app" {
  metadata {
    name = var.app_name
    namespace = var.namespace
    labels = {
      app = var.app_name
    }
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = var.app_name
      }
    }

    template {
      metadata {
        labels = {
          app = var.app_name
        }
      }

      spec {
        container {
          name  = "main"

          image = var.image
          image_pull_policy = "Always"

          env {
            name = "PORT"
            value = var.app_port
          }
          
          # env = {
          #   for name, value in var.env : name => value
          # }
          dynamic env {
            for_each = var.env
            content {
              name = env.value["name"]
              value = env.value["value"]
            }
          }

          resources {
            limits = {
              cpu    = "500m"
              memory = "256Mi"
            }
            requests = {
              cpu    = "200m"
              memory = "50Mi"
            }
          }

          readiness_probe {
            http_get {
              path = var.probe_path
              port = var.app_port
            }

            initial_delay_seconds = 1
            period_seconds        = 3
          }

          liveness_probe {
            http_get {
              path = var.probe_path
              port = var.app_port
            }

            initial_delay_seconds = 15
            period_seconds        = 20
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "svc" {
  metadata {
    name = var.svc_name
    namespace = var.namespace
  }
  spec {
    selector = {
      app = var.app_name
    }
    port {
      port        = 80
      target_port = var.app_port
    }

    type = "ClusterIP"
  }
}


resource "kubernetes_ingress" "api_ingress" {
  metadata {
    name = var.svc_name
    namespace = var.namespace

    annotations = {
      "cert-manager.io/cluster-issuer" = var.cert_issuer
      "kubernetes.io/ingress.class" = "nginx"
    }
  }

  spec {
    # backend {
    #   service_name = var.svc_name
    #   service_port = 80
    # }

    rule {
      host = "${var.ingress_domain}"
      http {
        path {
          backend {
            service_name = var.svc_name
            service_port = 80
          }

          path = var.ingress_path
        }
      }
    }
    rule {
      host = "${var.cdn_domain}"
      http {
        path {
          backend {
            service_name = var.svc_name
            service_port = 80
          }

          path = var.ingress_path
        }
      }
    }

    tls {
      secret_name = "${var.ingress_domain}-tls"
      hosts = ["${var.ingress_domain}"]
    }
  }
}