variable "region" {
  type = string
}
variable "project_id" {
  type = string
}
variable "project_name" {
  type = string
}
variable "project_base_domain" {
  type = string
}
variable "k8s_minor_version" {
  type = string
  default = "1.21"
}
variable "k8s_node_size" {
  type = string
  default = "s-2vcpu-2gb"
}
variable "k8s_min_nodes" {
  type = number
  default = 1
}
variable "k8s_max_nodes" {
  type = number
  default = 3
}
variable "cert_issuer" {
  type = string
  default = "letsencrypt-prod"
}
variable "backup_tools_image" {
  type = string
}
variable "do_access_key" {
  type = string
}
variable "do_access_key_scret" {
  type = string
}
variable "pg_backup_bucket" {
  type = string
}