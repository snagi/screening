terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.0.1"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

data "digitalocean_project" "project" {
  id = var.project_id
}

resource "digitalocean_project_resources" "project_resources" {
  project = data.digitalocean_project.project.id
  resources = [
    digitalocean_kubernetes_cluster.k8s_cluster1.urn,
    # digitalocean_database_cluster.db.urn
  ]
}

resource "digitalocean_container_registry" "cr" {
  name                   = "${var.project_name}-container-registry"
  subscription_tier_slug = "starter"
}

resource "digitalocean_vpc" "vpc1" {
  name   = "${var.region}-${var.project_name}-vpc-01"
  region = var.region
}

locals {
  cluster_name    = "${var.region}-${var.project_name}-k8s-01"
  db_cluster_name = "${var.region}-${var.project_name}-pg-01"
}

data "digitalocean_kubernetes_versions" "default" {
  version_prefix = "${var.k8s_minor_version}."
}

resource "digitalocean_kubernetes_cluster" "k8s_cluster1" {
  name         = local.cluster_name
  region       = var.region
  auto_upgrade = true
  version      = data.digitalocean_kubernetes_versions.default.latest_version
  vpc_uuid     = digitalocean_vpc.vpc1.id

  maintenance_policy {
    start_time = "04:00"
    day        = "sunday"
  }

  node_pool {
    name       = "default"
    auto_scale = true
    size       = var.k8s_node_size
    min_nodes  = var.k8s_min_nodes
    max_nodes  = var.k8s_max_nodes
  }
}

resource "digitalocean_spaces_bucket" "backup_bucket" {
  name   = var.pg_backup_bucket
  region = "nyc3"
}
module "kubernetes_configs" {
  source = "./kubernetes-configs"

  cluster_name        = local.cluster_name
  cluster_host        = digitalocean_kubernetes_cluster.k8s_cluster1.endpoint
  kube_config         = digitalocean_kubernetes_cluster.k8s_cluster1.kube_config[0]
  ingress_hostname    = var.project_base_domain
  cert_issuer         = var.cert_issuer
  backup_tools_image  = var.backup_tools_image
  region              = "nyc3"
  do_access_key       = var.do_access_key
  do_access_key_scret = var.do_access_key_scret
  backup_bucket_name  = digitalocean_spaces_bucket.backup_bucket.name
}

data "digitalocean_loadbalancer" "ingress_lb" {
  depends_on = [
    module.kubernetes_configs
  ]
  name = format("%s-nginx-ingress", local.cluster_name)
}

resource "digitalocean_domain" "screening" {
  name       = var.project_base_domain
  ip_address = data.digitalocean_loadbalancer.ingress_lb.ip
}

resource "digitalocean_record" "screening_wildcard" {
  domain = digitalocean_domain.screening.id
  type   = "A"
  name   = "*"
  value  = data.digitalocean_loadbalancer.ingress_lb.ip
}

# resource "digitalocean_database_cluster" "postgres" {
#   name       = local.db_cluster_name
#   engine     = "pg"
#   version    = "14"
#   size       = "db-s-1vcpu-2gb"
#   region     = var.region
#   node_count = 1
# }

# resource "digitalocean_database_db" "database" {
#   cluster_id = digitalocean_database_cluster.postgres.id
#   name       = var.project_name
# }

# resource "digitalocean_database_user" "user" {
#   cluster_id = digitalocean_database_cluster.postgres.id
#   name       = "dbuser"
# }

# resource "digitalocean_database_connection_pool" "default" {
#   cluster_id = digitalocean_database_cluster.postgres.id
#   name       = "pool-01"
#   mode       = "transaction"
#   size       = 20
#   db_name    = digitalocean_database_db.database.name
#   user       = digitalocean_database_user.user.name
# }
