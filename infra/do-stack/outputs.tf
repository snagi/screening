output "cluster_name" {
  value = local.cluster_name
}
output "cluster_host" {
  value = digitalocean_kubernetes_cluster.k8s_cluster1.endpoint
}
output "kube_config" {
  value = digitalocean_kubernetes_cluster.k8s_cluster1.kube_config[0]
}

# output "db_name" {
#   value = digitalocean_database_connection_pool.default.db_name
# }
# output "db_host" {
#   value = digitalocean_database_connection_pool.default.host
# }
# output "db_port" {
#   value = digitalocean_database_connection_pool.default.port
# }
# output "db_user" {
#   value = digitalocean_database_connection_pool.default.user
# }
# output "db_password" {
#   value = digitalocean_database_connection_pool.default.password
# }

output "db_name" {
  value = "pgdefaultdb"
}
output "db_host" {
  value = "postgres-postgresql-ha-postgresql.postgres"
}
output "db_port" {
  value = "5432"
}
output "db_user" {
  value = "pguser"
}
output "db_password" {
  value = "password"
}