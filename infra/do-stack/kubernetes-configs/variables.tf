variable "cluster_host" {
  type = string
}
variable "cluster_name" {
  type = string
}
variable "kube_config" {
  type = object({
   token = string,
   cluster_ca_certificate = string,
  })
}
variable "ingress_hostname" {
  type = string
}
variable "cert_issuer" {
  type = string
}

variable "pg_database" {
  type = string
  default = "pgdefaultdb"
}
variable "pg_username" {
  type = string
  default = "repuser"
}
variable "pg_password" {
  type = string
  default = "password"
}
variable "backup_tools_image" {
  type = string
}

variable "do_access_key" {
  type = string
}
variable "do_access_key_scret" {
  type = string
}
variable "region" {
  type = string
}
variable "backup_bucket_name" {
  type = string
}