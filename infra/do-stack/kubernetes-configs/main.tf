terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.0.1"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

provider "kubernetes" {
  host  = var.cluster_host
  token = var.kube_config.token
  cluster_ca_certificate = base64decode(
    var.kube_config.cluster_ca_certificate
  )
}

provider "helm" {
  kubernetes {
    host  = var.cluster_host
    token = var.kube_config.token
    cluster_ca_certificate = base64decode(
      var.kube_config.cluster_ca_certificate
    )
  }
}

provider "kubectl" {
  host  = var.cluster_host
  token = var.kube_config.token
  cluster_ca_certificate = base64decode(
    var.kube_config.cluster_ca_certificate
  )
  load_config_file = false
}

resource "kubernetes_namespace" "ingress" {
  metadata {
    name = "ingress"
  }
}

resource "helm_release" "nginx_ingress" {
  name      = "nginx-ingress-controller"
  namespace = kubernetes_namespace.ingress.metadata.0.name

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx-ingress-controller"

  set {
    name  = "service.type"
    value = "LoadBalancer"
  }
  set {
    name  = "service.annotations.service\\.beta\\.kubernetes\\.io/do-loadbalancer-name"
    value = format("%s-nginx-ingress", var.cluster_name)
  }
  set {
    name  = "service.annotations.service\\.beta\\.kubernetes\\.io/do-loadbalancer-hostname"
    value = var.ingress_hostname
  }
  timeout = 600
}

data "digitalocean_loadbalancer" "ingress_lb" {
  depends_on = [
    helm_release.nginx_ingress
  ]
  name = format("%s-nginx-ingress", var.cluster_name)
}

resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert_manager" {
  depends_on = [
    helm_release.nginx_ingress
  ]

  name      = "cert-manager"
  namespace = kubernetes_namespace.cert_manager.metadata.0.name

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "1.7.1"

  set {
    name  = "installCRDs"
    value = "true"
  }

  timeout = 600
}

resource "kubectl_manifest" "letsencrypt-cluster-issuer" {
  depends_on = [
    helm_release.cert_manager
  ]
  yaml_body = <<YAML
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: ${var.cert_issuer}
spec:
  acme:
    # The ACME server URL
    server: https://acme-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: sushil.nagi@gmail.com
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: ${var.cert_issuer}-secret
    # Enable the HTTP-01 challenge provider
    solvers:
    - http01:
        ingress:
           class: nginx
YAML
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

locals {
  prometheus_values = templatefile(
    "${path.module}/prometheus-values.yaml.tftpl",
    { base_domain : var.ingress_hostname, cert_issuer : var.cert_issuer, ingress_class : "nginx" }
  )
  loki_values = templatefile(
    "${path.module}/loki-values.yaml.tftpl",
    { base_domain : var.ingress_hostname, cert_issuer : var.cert_issuer, ingress_class : "nginx" }
  )
  postgres_values = templatefile(
    "${path.module}/postgres-values.yaml.tftpl",
    { database : var.pg_database, username : var.pg_username, password : var.pg_password }
  )
}

resource "helm_release" "kube_prometheus_stack" {
  depends_on = [
    helm_release.nginx_ingress
  ]

  name      = "kube-prometheus-stack"
  namespace = kubernetes_namespace.monitoring.metadata.0.name

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"

  values = [
    "${local.prometheus_values}"
  ]

  timeout = 600
}

resource "kubernetes_namespace" "postgres" {
  metadata {
    name = "postgres"
  }
}
resource "helm_release" "postgres" {
  name      = "postgres"
  namespace = kubernetes_namespace.postgres.metadata.0.name

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql-ha"

  values = [
    "${local.postgres_values}"
  ]

  timeout = 600
}

resource "kubernetes_cron_job" "postgres-backup" {
  metadata {
    name      = "postgres-backup"
    namespace = kubernetes_namespace.postgres.metadata.0.name
  }
  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 5
    schedule                      = "1 * * * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10
    job_template {
      metadata {}
      spec {
        backoff_limit              = 2
        ttl_seconds_after_finished = 10
        template {
          metadata {}
          spec {
            container {
              name              = "backup-tools"
              image             = var.backup_tools_image
              image_pull_policy = "Always"

              args = ["/bin/bash", "-c", "/postgres-backup.sh;"]

              env {
                name  = "AWS_ACCESS_KEY_ID"
                value = var.do_access_key
              }
              env {
                name  = "AWS_SECRET_ACCESS_KEY"
                value = var.do_access_key_scret
              }
              env {
                name  = "AWS_DEFAULT_REGION"
                value = var.region
              }
              env {
                name  = "BUCKET_NAME"
                value = "s3://${var.backup_bucket_name}"
              }
              env {
                name  = "DB"
                value = var.pg_database
              }
              env {
                name  = "DBHOST"
                value = "postgres-postgresql-ha-postgresql"
              }
              env {
                name  = "DBPORT"
                value = 5432
              }
              env {
                name  = "DBUSER"
                value = var.pg_username
              }
              env {
                name  = "DBPASS"
                value = var.pg_password
              }
              env {
                name  = "POSTGRES_PASSWORD"
                value = var.pg_password
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "psql-client" {
  metadata {
    name      = "psql-client"
    namespace = kubernetes_namespace.postgres.metadata.0.name
    labels = {
      app = "psql-client"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "psql-client"
      }
    }

    template {
      metadata {
        labels = {
          app = "psql-client"
        }
      }

      spec {
        container {
          name = "main"

          image             = "postgres:latest"
          image_pull_policy = "Always"

          env {
            name  = "POSTGRES_PASSWORD"
            value = "password"
          }

          resources {
            limits = {
              cpu    = "500m"
              memory = "256Mi"
            }
            requests = {
              cpu    = "100m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_namespace" "loki-stack" {
  metadata {
    name = "loki-stack"
  }
}
resource "helm_release" "loki" {
  name      = "loki"
  namespace = kubernetes_namespace.loki-stack.metadata.0.name

  repository = "https://grafana.github.io/loki/charts"
  chart      = "loki-stack"

  values = [
    "${local.loki_values}"
  ]

  timeout = 600
}
