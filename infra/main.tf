terraform {
  cloud {
    organization = "snagi"

    workspaces {
      name = "dev"
    }
  }

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.0.1"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}
provider "digitalocean" {}

provider "cloudflare" {}

resource "digitalocean_project" "screening" {
  name        = var.project_name
  description = "A project to represent development resources."
  purpose     = "3 Tier App"
  environment = "Development"
}

module "stack_resources" {
  # depends_on = [
  #   digitalocean_project.screening
  # ]
  source = "./do-stack"

  region = "lon1"
  project_id = digitalocean_project.screening.id
  project_name = var.project_name
  project_base_domain = "${var.project_name}.snagi.org"
  cert_issuer = "letsencrypt-prod"

  k8s_min_nodes = 3
  backup_tools_image = var.BACKUP_TOOLS_IMAGE

  do_access_key = var.SPACES_ACCESS_KEY_ID
  do_access_key_scret = var.SPACES_SECRET_ACCESS_KEY
  pg_backup_bucket = var.PG_BACKUP_BUCKET
}

module "apps" {
  source = "./kubernetes-apps"

  cluster_name = module.stack_resources.cluster_name
  cluster_host = module.stack_resources.cluster_host
  kube_config = module.stack_resources.kube_config
  ingress_hostname = "${var.project_name}.snagi.org"
  cert_issuer = "letsencrypt-prod"

  namespace = "screening-apps-prod"

  db_name = module.stack_resources.db_name
  db_host = module.stack_resources.db_host
  db_port = module.stack_resources.db_port
  db_user = module.stack_resources.db_user
  db_password = module.stack_resources.db_password

  web_image = var.WEB_IMAGE
  api_image = var.API_IMAGE
  backup_tools_image = var.BACKUP_TOOLS_IMAGE
}

data "cloudflare_zones" "domain" {
  filter {
    name = var.site_domain
  }
}
resource "cloudflare_record" "screening" {
  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = "screening"
  value   = module.apps.web_host
  type    = "CNAME"

  ttl     = 1
  proxied = true
}

resource "cloudflare_record" "screening-api" {
  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = "screening-api"
  value   = module.apps.api_host
  type    = "CNAME"

  ttl     = 1
  proxied = true
}
